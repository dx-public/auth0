#!/bin/env bash

#get current dir
cdir=`dirname $0`

#check status
[ $(git rev-parse HEAD) = $(git ls-remote $(git rev-parse --abbrev-ref @{u} | sed 's/\// /g') | cut -f1) ] && changed=0 || changed=1
if [ $changed = 1 ]; then
 #update git-repo first
 #git reset -- hard # for resetting
 echo "Updating script.."
 git -C $cdir pull
 echo "Please rerun login.sh"
 exit
fi

ls /root/ 1>/dev/null 2>&1 || (echo "Please run as root" && exit)

#Checking for utilities
#echo "Checking if utilities are installed"
#installedjq=`dpkg-query -l | awk '{ print $2 }' | grep -e '^jq'`
#if [ -z "$installedjq" ]; then apt install -y jq; fi

 if ! which jq > /dev/null; then
  echo "" #not installed
  apt install -y jq
  else
  echo "" #installed
  fi

#installedkdig=`dpkg-query -l | awk '{ print $2 }' | grep -e '^knot-dnsutils'`
#if [ -z "$installedkdig" ]; then apt install -y knot-dnsutils; fi


MODEL=`cat /sys/class/dmi/id/product_name | tr -dc '[:alnum:]\n\r'`
PETH=`ls -l /sys/class/net | grep -v 'virtual' | rev | cut -d '/' -f1 | rev | tail -n -1`
MAC=`cat /sys/class/net/$PETH/address | sort -r | head -n 1 | cut -d':' -f4- | tr -d ':'`
USRAGT=`echo ${MODEL}-${MAC}`


#Get Config
#if [ -e /root/.key ]; then echo "Run Util"; fi

#CFG_AUTH0_DNS=`kdig @9.9.9.9 +tls +short TXT cfg-auth0.$(hostname -f | cut -d'.' -f2-)`

CFG_AUTH0_DNS=`curl -s -H 'accept: application/dns-json' "https://1.1.1.1/dns-query?name=cfg-auth0.$(hostname -f | cut -d'.' -f2-)&type=TXT" | jq .Answer[0].data`
AUTH0_DOMAIN=`echo $CFG_AUTH0_DNS | cut -d '|' -f1 | tr -d '"' | tr -d '\\\\' | sed 's/$/.auth0.com/'`
AUTH0_CLIENT_ID=`echo $CFG_AUTH0_DNS | cut -d'.' -f2- | cut -d '|' -f2 | tr -d '"' | tr -d '\\\\'`

[ -z "${AUTH0_DOMAIN}" ] && { echo >&2 "ERROR: AUTH0_DOMAIN undefined"; exit 1; }
[ -z "${AUTH0_CLIENT_ID}" ] && { echo >&2 "ERROR: AUTH0_CLIENT_ID undefined"; exit 1; }

[ -n "${opt_mgmnt}" ] && audience_field=",\"audience\":\"https://${AUTH0_DOMAIN}/api/v2/\""

BODY=$(cat <<EOL
{
    "client_id":"${AUTH0_CLIENT_ID}",
    "scope":"openid offline_access profile"
    ${audience_field}
}
EOL
)

CODE=`curl -ss --user-agent ${USRAGT} --header 'content-type: application/json' -d "${BODY}" https://${AUTH0_DOMAIN}/oauth/device/code | jq .`

USER_CODE=`echo $CODE | jq .user_code | tr -d '"'` #XXXX-XXXX
DEVICE_CODE=`echo $CODE | jq .device_code | tr -d '"'` #LONG STRING
VERIFICATION_URI_COMPLETE=`echo $CODE | jq .verification_uri_complete | tr -d '"'` #https://${AUTH0_DOMAIN}/activate?user-code=XXXX-XXXX
VERIFICATION_URI=`echo $CODE | jq .verification_uri | tr -d '"'` #https://${AUTH0_DOMAIN}/activate

echo "*******************************************************************"
echo "Your Device Code is: $DEVICE_CODE"
echo "Please use a Web browser on phone/computer and navigate to $VERIFICATION_URI and then input code $USER_CODE or click the link below"
echo "$VERIFICATION_URI_COMPLETE"
echo "*******************************************************************"

_math() {
  _m_opts="$@"
  printf "%s" "$(($_m_opts))"
}

__green() {
    printf '\33[1;32m%b\33[0m' "$1"
    return
}

__red() {
    printf '\33[1;31m%b\33[0m' "$1"
    return
}

__white() {
  printf -- "%b" "$1"
  return
}

# sleep sec
_sleep() {
  _sleep_sec="$1"
    _sleep_c="$_sleep_sec"
    while [ "$_sleep_c" -gt "0" ]; do
      printf "\r      \r"
      if [ "$_sleep_c" -gt "3" ]; then __green "$_sleep_c"; fi
      if [ "$_sleep_c" -le "3" ]; then __red "$_sleep_c"; fi
      if [ "$_sleep_c" -eq "0" ]; then __white "$_sleep_c"; fi
      _sleep_c="$(_math "$_sleep_c" - 1)"
      sleep 1
    done
    printf "\r"
}


#echo "Please press enter after completing the steps above"
#read
echo "Please click the link within 60sec.."
_sleep 60
stty sane #reset terminal colors

BODY=$(cat <<EOL
{
    "client_id":"${AUTH0_CLIENT_ID}",
    "device_code":"${DEVICE_CODE}",
    "grant_type":"urn:ietf:params:oauth:grant-type:device_code"
}
EOL
)


TOKEN=`curl -ss --user-agent ${USRAGT} --header 'content-type: application/json' -d "${BODY}" https://${AUTH0_DOMAIN}/oauth/token | jq .`

ACCESS_TOKEN=`echo $TOKEN | jq .access_token | tr -d '"'`
REFRESH_TOKEN=`echo $TOKEN | jq .refresh_token`
ID_TOKEN=`echo $TOKEN | jq .id_token`
SCOPE=`echo $TOKEN | jq .scope`


USER_INFO=`curl -ss --request GET --url https://${AUTH0_DOMAIN}/userinfo --header "authorization: Bearer ${ACCESS_TOKEN}" --header 'content-type: application/json'`

if [ "$USER_INFO" = 'Unauthorized' ]; then echo 'timeout... exiting'; exit; fi
echo
echo
echo "Logged in as: $( echo ${USER_INFO} | jq .name | tr -d '"')"
echo $USER_INFO | jq .

echo $USER_INFO  | openssl enc -aes-256-cbc -md sha512 -a -pbkdf2 -iter 100000 -salt -pass pass:$USRAGT >/root/.user_info
chmod 600 /root/.user_info
#echo $USER_INFO > /root/.user_info


